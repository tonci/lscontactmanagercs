C# and JavaScript CODE for [Beginning LightSwitch in Visual Studio 2013 - Address Book HTML Sample](https://code.msdn.microsoft.com/Beginning-LightSwitch-in-a7f58003) by [Beth Massi](https://blogs.msdn.microsoft.com/bethmassi/tag/lightswitch/)

- updated to VS2015

#Original description: [Beginning LightSwitch in VS 2013 - Address Book HTML Sample](assets/description.html)
If you are completely new to LightSwitch or HTML/JavaScript development then this tutorial is for you. It walks you through everything you need to know to get started quickly building professional quality business apps for mobile devices & tablets, suitable for a wide range of form factors. Read the step-by-step tutorials in the Beginning LightSwitch in Visual Studio 2013 Article Series.

- [Part 1: What’s in a Table? Describing Your Data](http://blogs.msdn.com/b/bethmassi/archive/2013/10/16/beginning-lightswitch-in-vs-2013-part-1-what-s-in-a-table-describing-your-data.aspx)
- [Part 2: Feel the Love - Defining Data Relationships](http://blogs.msdn.com/b/bethmassi/archive/2013/10/23/beginning-lightswitch-in-vs-2013-part-2-feel-the-love-defining-data-relationships.aspx)
- [Part 3: Screen Templates, Which One Do I Choose?](http://blogs.msdn.com/b/bethmassi/archive/2013/11/04/beginning-lightswitch-in-vs-2013-part-3-screen-templates-which-one-do-i-choose.aspx)
- [Part 4: Too much information! Sorting and Filtering Data with Queries](http://blogs.msdn.com/b/bethmassi/archive/2013/11/27/beginning-lightswitch-in-vs-2013-part-4-too-much-information-sorting-and-filtering-data-with-queries.aspx)
- [Part 5: May I? Controlling Access with User Permissions](http://blogs.msdn.com/b/bethmassi/archive/2013/12/18/beginning-lightswitch-in-vs-2013-part-5-may-i-controlling-access-with-user-permissions.aspx)
- [Part 6: More Control! Customizing the app with JavaScript & CSS](http://blogs.msdn.com/b/bethmassi/archive/2014/01/29/beginning-lightswitch-in-vs-2013-part-6-more-control-customizing-the-app-with-javascript-amp-css.aspx)

##Building the Sample
Download the ZIP to your computer, right-click on the ZIP, select properties, and Unblock the file.
Then extract the contents of the ZIP and double-click on the .SLN file to open Visual Studio.
Important: From the BUILD menu, select "Rebuild Solution" to generate the necessary files to run the sample.
After you rebuild, press F5 to run.

##More Information
Visual Studio LightSwitch is a development environment designed to simplify and shorten the development of businesses applications and data services. LightSwitch makes it easy to create data-centric business applications that can consume a variety of data sources and create clients that can run on a variety of devices. With LightSwitch you can:

##Build HTML5-based apps that run on all modern devices
Consume and aggregate multiple data sources like databases, SharePoint and OData
Eliminate plumbing code and focus on what makes your application unique
Have flexible deployment and cloud hosting options including Azure and Office 365
Trust that your solution is built on best-of-breed technologies and practices
Visit the LightSwitch section of the Visual Studio Developer Center to download Visual Studio and get started learning.

#[Original licence](assets/license.rtf)

Copyright 2017 Tonci Vatavuk Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.